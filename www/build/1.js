webpackJsonp([1],{

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RelayboardPageModule", function() { return RelayboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__relayboard__ = __webpack_require__(284);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RelayboardPageModule = /** @class */ (function () {
    function RelayboardPageModule() {
    }
    RelayboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__relayboard__["a" /* RelayboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__relayboard__["a" /* RelayboardPage */]),
            ],
        })
    ], RelayboardPageModule);
    return RelayboardPageModule;
}());

//# sourceMappingURL=relayboard.module.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RelayboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the RelayboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RelayboardPage = /** @class */ (function () {
    function RelayboardPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RelayboardPage.prototype.ionViewDidLoad = function () {
        console.log('RelayboardPage');
    };
    RelayboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-relayboard',template:/*ion-inline-start:"/home/tech/Dev/unTest/src/pages/relayboard/relayboard.html"*/'<!--\n  Generated template for the RelayboardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Actions</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list [virtualScroll]="actions" [approxItemHeight]="\'88px\'">\n    <ion-card *virtualItem="let action">\n        <ion-item>\n          <ion-grid>\n            <ion-row>\n              <ion-col col-10 class="container_parent">\n                <div class="container_child">\n                  <p class="content_text">Nom : {{ action.nom_action }}</p>\n                  <p class="content_text">N° Relais : {{ action.num_relais }}</p>\n                  <p class="content_text">Type d\'action : {{ action.type_action }}</p>\n                </div>\n              </ion-col>\n              <ion-col  col-2 class="container_parent">\n                <div class="container_child">\n                  <button ion-button (click)="supp(action.id_action)" class="supp">X</button>\n                </div>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col  col-2 class="container_parent">\n                <div class="container_child">\n                  <button ion-button (click)="modif(action.id_action, action.num_relais, action.nom_action, action.type_action)" class="modif">EDIT</button>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-item>\n      </ion-card>\n    </ion-list>\n\n\n\n\n\n  <ion-fab right bottom>\n     <button ion-fab color="red" (click)="newAction()"><ion-icon name="add"></ion-icon></button>\n   </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/home/tech/Dev/unTest/src/pages/relayboard/relayboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], RelayboardPage);
    return RelayboardPage;
}());

//# sourceMappingURL=relayboard.js.map

/***/ })

});
//# sourceMappingURL=1.js.map