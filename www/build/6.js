webpackJsonp([6],{

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncidentPageModule", function() { return IncidentPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__incident__ = __webpack_require__(279);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var IncidentPageModule = /** @class */ (function () {
    function IncidentPageModule() {
    }
    IncidentPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__incident__["a" /* IncidentPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__incident__["a" /* IncidentPage */]),
            ],
        })
    ], IncidentPageModule);
    return IncidentPageModule;
}());

//# sourceMappingURL=incident.module.js.map

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IncidentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the IncidentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var IncidentPage = /** @class */ (function () {
    function IncidentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        console.log("incident");
    }
    IncidentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-incident',template:/*ion-inline-start:"/home/tech/Dev/unTest/src/pages/incident/incident.html"*/'<!--\n  Generated template for the IncidentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Incident</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label>Titre : </ion-label>\n          <ion-input [(ngModel)]="title"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label>Contenu : </ion-label>\n          <ion-textarea class="input-message" rows="6" [(ngModel)]="content" autosize></ion-textarea>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-item>\n          <ion-label>Destinataire : </ion-label>\n          <ion-select [(ngModel)]="emailDest">\n            <ion-option *ngFor="let mail of emails" value="{{mail.mail}}">{{mail.mail}}</ion-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row align-items-center>\n      <ion-col class="container_parent" col-4>\n        <div class="container_child">\n          <p>Photo :</p>\n        </div>\n      </ion-col>\n      <ion-col class="container_parent" col-8>\n        <div class="container_child">\n          <ion-img width="50" height="50" style="background: transparent !important" src="assets/imgs/button/photo-camera.png" (click)="captureImage()"></ion-img>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <!--Si le tableau qui contient les photos est vide, on affiche pas le slider pour une meilleur ergonomie-->\n      <ion-col col-12 *ngIf="empty_tab == false">\n        <ion-slides #mySlider class="image-slider" loop="false" slidesPerView="4">\n          <ion-slide *ngFor="let img of images">\n            <!--Si img est null, on affiche pas la balise-->\n            <img *ngIf="img != null" ion-long-press [interval]="400" (onPressStart)="AskRemovePicture(img)" src="data:image/jpg;base64,{{img}}" class="thumb-img"  imageViewer/>\n          </ion-slide>\n        </ion-slides>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class="container_parent" col-12>\n        <div class="container_child">\n          <button ion-button class="button"  (click)="sendEmail()">Envoyer</button>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n<!--\n   <ion-card>\n     <ion-card-header>Current image</ion-card-header>\n     <ion-card-content>\n       <img [src]="currentImage" *ngIf="currentImage" />\n     </ion-card-content>\n   </ion-card>\n-->\n\n</ion-content>\n'/*ion-inline-end:"/home/tech/Dev/unTest/src/pages/incident/incident.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], IncidentPage);
    return IncidentPage;
}());

//# sourceMappingURL=incident.js.map

/***/ })

});
//# sourceMappingURL=6.js.map