import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad(){

  }

  actions(){
    this.navCtrl.push("ActionPage", {

    }).catch((error: any) => {

    });
  }

  bennes(){
	this.navCtrl.push("BennesPage", {

    }).catch((error: any) => {

    });
  }

}
